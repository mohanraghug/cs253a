#!/bin/bash
rm -rf tests
mkdir tests

g++ main/generator.cpp -o generator

n=$1

for ((i = 1;i<=n;i++)) do
  ./generator > tests/$i.in
done

rm generator
