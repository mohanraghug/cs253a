import csv
import matplotlib.pyplot as plt
import numpy as np

odi_matches_vs_year = {}
odi_runs_vs_year = {}
odi_dismissal_types = {}
odi_dismissal_scores = []
odi_avg_vs_years = {}
odi_sr_vs_years = {}
odi_100s_vs_year = {}
odi_50s_vs_year = {}
odi_6s_vs_year = {}
odi_4s_vs_year = {}
odi_batting_pos_runs = {}
odi_oppn_vs_runs = {}


with open('data/odis.csv') as odis:
    csv_reader = csv.reader(odis, delimiter=',')
    flag = False
    for row in csv_reader:
        if flag:
            if row[2] == '-':
                continue
            year = int(row[-2][-4:])
            runs = row[1]
            pos = int(row[7])
            if runs[-3:] == "DNB":
                continue
            if runs[-1] == '*':
                runs = runs[:-1]
            odi_dismissal_scores.append(int(runs))
            if pos in odi_batting_pos_runs:
                odi_batting_pos_runs[pos] += int(runs)
            else:
                odi_batting_pos_runs[pos] = int(runs)

            if row[8] != "not out" and row[8] != "-" and row[8] != "retired notout":
                if row[8] in odi_dismissal_types:
                    odi_dismissal_types[row[8]] += 1
                else:
                    odi_dismissal_types[row[8]] = 1

        flag = True


with open('data/odis_cumulative.csv') as odis:
    csv_reader = csv.reader(odis, delimiter=',')
    flag = False
    for row in csv_reader:
        if flag:
            year = int(row[-2][-4:])
            matches = row[1]
            runs = row[4]
            ave = row[6]
            sr = row[8]
            huns = row[9]
            fifs = row[10]
            fours = row[12]
            sixes = row[13]
            odi_matches_vs_year[year] = int(matches)
            odi_runs_vs_year[year] = int(runs)
            odi_avg_vs_years[year] = float(ave)
            odi_sr_vs_years[year] = float(sr)
            odi_100s_vs_year[year] = int(huns)
            odi_50s_vs_year[year] = int(fifs)
            odi_4s_vs_year[year] = int(fours)
            odi_6s_vs_year[year] = int(sixes)

        flag = True


plt.figure()
plt.hist(odi_dismissal_scores)
plt.title('Histogram of Scores in ODI')
plt.xlabel('Score')
plt.ylabel('Frequency of Dismissals')
plt.savefig('imgs/odi_scores.png')
plt.close()

plt.figure(figsize=(10, 8))
plt.plot(list(odi_avg_vs_years.keys()), list(
    odi_avg_vs_years.values()), marker='o')
plt.xticks(list(odi_avg_vs_years.keys()))
plt.xlabel('Year')
plt.ylabel('Average')
plt.title('Batting Average in ODIs over the years')
plt.savefig('imgs/odi_batting_avg.png')
plt.close()

plt.figure(figsize=(10, 8))
plt.plot(list(odi_sr_vs_years.keys()), list(
    odi_sr_vs_years.values()), marker='o')
plt.xticks(list(odi_sr_vs_years.keys()))
plt.xlabel('Year')
plt.ylabel('Strike Rate')
plt.title('Batting Strike Rate in ODIs over the years')
plt.savefig('imgs/odi_batting_sr.png')
plt.close()

odi_runs = list(odi_runs_vs_year.values())
years = list(odi_runs_vs_year.keys())

for i in range(len(odi_runs)-1, 0, -1):
    odi_runs[i] -= odi_runs[i - 1]

plt.figure(figsize=(10, 8))
plt.bar(years, odi_runs)
plt.xticks(years)
plt.xlabel('Year')
plt.ylabel('Runs')
plt.title('ODI runs vs year')
plt.savefig('imgs/odi_runs_vs_year.png')
plt.close()

plt.figure()
plt.pie(list(odi_dismissal_types.values()), labels=list(
    odi_dismissal_types.keys()), autopct='%1.2f%%')
plt.title('Dismissal types in ODIs')
plt.savefig('imgs/odi_dismissals.png')
plt.close()

plt.figure()
plt.bar(list(odi_batting_pos_runs.keys()), list(odi_batting_pos_runs.values()))
plt.xticks(list(odi_batting_pos_runs.keys()))
plt.title('Batting Position vs Runs in ODIs')
plt.xlabel('Batting Position')
plt.ylabel('Runs')
plt.savefig('imgs/odi_position_vs_runs.png')
plt.close()


test_matches_vs_year = {}
test_runs_vs_year = {}
test_dismissal_types = {}
test_dismissal_scores = []
test_avg_vs_years = {}
test_sr_vs_years = {}
test_100s_vs_year = {}
test_50s_vs_year = {}
test_6s_vs_year = {}
test_4s_vs_year = {}
test_batting_pos_runs = {}
test_oppn_vs_runs = {}


with open('data/tests.csv') as tests:
    csv_reader = csv.reader(tests, delimiter=',')
    flag = False
    for row in csv_reader:
        if flag:
            if row[2] == '-':
                continue
            year = int(row[-2][-4:])
            runs = row[1]
            pos = int(row[7])
            if runs[-3:] == "DNB":
                continue
            if runs[-1] == '*':
                runs = runs[:-1]
            test_dismissal_scores.append(int(runs))
            if pos in test_batting_pos_runs:
                test_batting_pos_runs[pos] += int(runs)
            else:
                test_batting_pos_runs[pos] = int(runs)

            if row[8] != "not out" and row[8] != "-" and row[8] != "retired notout":
                if row[8] in test_dismissal_types:
                    test_dismissal_types[row[8]] += 1
                else:
                    test_dismissal_types[row[8]] = 1

        flag = True


with open('data/tests_cumulative.csv') as tests:
    csv_reader = csv.reader(tests, delimiter=',')
    flag = False
    for row in csv_reader:
        if flag:
            year = int(row[-2][-4:])
            matches = row[1]
            runs = row[4]
            ave = row[6]
            sr = row[8]
            huns = row[9]
            fifs = row[10]
            fours = row[12]
            sixes = row[13]
            test_matches_vs_year[year] = int(matches)
            test_runs_vs_year[year] = int(runs)
            test_avg_vs_years[year] = float(ave)
            test_sr_vs_years[year] = float(sr)
            test_100s_vs_year[year] = int(huns)
            test_50s_vs_year[year] = int(fifs)
            test_4s_vs_year[year] = int(fours)
            test_6s_vs_year[year] = int(sixes)

        flag = True


plt.figure()
plt.hist(test_dismissal_scores)
plt.title('Histogram of Scores in test')
plt.xlabel('Score')
plt.ylabel('Frequency of Dismissals')
plt.savefig('imgs/test_scores.png')
plt.close()

plt.figure(figsize=(10, 8))
plt.plot(list(test_avg_vs_years.keys()), list(
    test_avg_vs_years.values()), marker='o')
plt.xticks(list(test_avg_vs_years.keys()))
plt.xlabel('Year')
plt.ylabel('Average')
plt.title('Batting Average in tests over the years')
plt.savefig('imgs/test_batting_avg.png')
plt.close()

plt.figure(figsize=(10, 8))
plt.plot(list(test_sr_vs_years.keys()), list(
    test_sr_vs_years.values()), marker='o')
plt.xticks(list(test_sr_vs_years.keys()))
plt.xlabel('Year')
plt.ylabel('Strike Rate')
plt.title('Batting Strike Rate in tests over the years')
plt.savefig('imgs/test_batting_sr.png')
plt.close()

test_runs = list(test_runs_vs_year.values())
years = list(test_runs_vs_year.keys())

for i in range(len(test_runs)-1, 0, -1):
    test_runs[i] -= test_runs[i - 1]


plt.figure(figsize=(10, 8))
plt.bar(years, test_runs)
plt.xticks(years)
plt.xlabel('Year')
plt.ylabel('Runs')
plt.title('Test runs vs year')
plt.savefig('imgs/test_runs_vs_year.png')
plt.close()

plt.figure()
plt.pie(list(test_dismissal_types.values()), labels=list(
    test_dismissal_types.keys()), autopct='%1.2f%%')
plt.title('Dismissal types in tests')
plt.savefig('imgs/test_dismissals.png')
plt.close()

plt.figure()
plt.bar(list(test_batting_pos_runs.keys()),
        list(test_batting_pos_runs.values()))
plt.xticks(list(test_batting_pos_runs.keys()))
plt.title('Batting Position vs Runs in tests')
plt.xlabel('Batting Position')
plt.ylabel('Runs')
plt.savefig('imgs/test_position_vs_runs.png')
plt.close()


t20_matches_vs_year = {}
t20_runs_vs_year = {}
t20_dismissal_types = {}
t20_dismissal_scores = []
t20_avg_vs_years = {}
t20_sr_vs_years = {}
t20_100s_vs_year = {}
t20_50s_vs_year = {}
t20_6s_vs_year = {}
t20_4s_vs_year = {}
t20_batting_pos_runs = {}
t20_oppn_vs_runs = {}


with open('data/t20s.csv') as t20s:
    csv_reader = csv.reader(t20s, delimiter=',')
    flag = False
    for row in csv_reader:
        if flag:
            if row[2] == '-':
                continue
            year = int(row[-2][-4:])
            runs = row[1]
            pos = int(row[7])
            if runs[-3:] == "DNB":
                continue
            if runs[-1] == '*':
                runs = runs[:-1]
            t20_dismissal_scores.append(int(runs))
            if pos in t20_batting_pos_runs:
                t20_batting_pos_runs[pos] += int(runs)
            else:
                t20_batting_pos_runs[pos] = int(runs)

            if row[8] != "not out" and row[8] != "-" and row[8] != "retired notout":
                if row[8] in t20_dismissal_types:
                    t20_dismissal_types[row[8]] += 1
                else:
                    t20_dismissal_types[row[8]] = 1

        flag = True


with open('data/t20s_cumulative.csv') as t20s:
    csv_reader = csv.reader(t20s, delimiter=',')
    flag = False
    for row in csv_reader:
        if flag:
            year = int(row[-2][-4:])
            matches = row[1]
            runs = row[4]
            ave = row[6]
            sr = row[8]
            huns = row[9]
            fifs = row[10]
            fours = row[12]
            sixes = row[13]
            t20_matches_vs_year[year] = int(matches)
            t20_runs_vs_year[year] = int(runs)
            if ave != '-':
                t20_avg_vs_years[year] = float(ave)
            t20_sr_vs_years[year] = float(sr)
            t20_100s_vs_year[year] = int(huns)
            t20_50s_vs_year[year] = int(fifs)
            t20_4s_vs_year[year] = int(fours)
            t20_6s_vs_year[year] = int(sixes)

        flag = True


plt.figure()
plt.hist(t20_dismissal_scores)
plt.title('Histogram of Scores in t20')
plt.xlabel('Score')
plt.ylabel('Frequency of Dismissals')
plt.savefig('imgs/t20_scores.png')
plt.close()

plt.figure(figsize=(10, 8))
plt.plot(list(t20_avg_vs_years.keys()), list(
    t20_avg_vs_years.values()), marker='o')
plt.xticks(list(t20_avg_vs_years.keys()))
plt.xlabel('Year')
plt.ylabel('Average')
plt.title('Batting Average in t20s over the years')
plt.savefig('imgs/t20_batting_avg.png')
plt.close()

plt.figure(figsize=(10, 8))
plt.plot(list(t20_sr_vs_years.keys()), list(
    t20_sr_vs_years.values()), marker='o')
plt.xticks(list(t20_sr_vs_years.keys()))
plt.xlabel('Year')
plt.ylabel('Strike Rate')
plt.title('Batting Strike Rate in t20s over the years')
plt.savefig('imgs/t20_batting_sr.png')
plt.close()

t20_runs = list(t20_runs_vs_year.values())
years = list(t20_runs_vs_year.keys())

for i in range(len(t20_runs)-1, 0, -1):
    t20_runs[i] -= t20_runs[i - 1]


plt.figure(figsize=(10, 8))
plt.bar(years, t20_runs)
plt.xticks(years)
plt.xlabel('Year')
plt.ylabel('Runs')
plt.title('T20I runs vs year')
plt.savefig('imgs/t20_runs_vs_year.png')
plt.close()

plt.figure()
plt.pie(list(t20_dismissal_types.values()), labels=list(
    t20_dismissal_types.keys()), autopct='%1.2f%%')
plt.title('Dismissal types in t20s')
plt.savefig('imgs/t20_dismissals.png')
plt.close()

plt.figure()
plt.bar(list(t20_batting_pos_runs.keys()), list(t20_batting_pos_runs.values()))
plt.xticks(list(t20_batting_pos_runs.keys()))
plt.title('Batting Position vs Runs in t20s')
plt.xlabel('Batting Position')
plt.ylabel('Runs')
plt.savefig('imgs/t20_position_vs_runs.png')
plt.close()

odi_runs = []
test_runs = []
t20_runs = []
years = []
odi = 0
test = 0
t20 = 0
for i in range(2007, 2022):
    if i not in odi_runs_vs_year.keys():
        odi_runs.append(0)
    else:
        foo = odi_runs_vs_year[i]
        odi_runs.append(foo - odi)
        odi = foo
    if i not in t20_runs_vs_year.keys():
        t20_runs.append(0)
    else:
        foo = t20_runs_vs_year[i]
        t20_runs.append(foo - t20)
        t20 = foo
    if i not in test_runs_vs_year.keys():
        test_runs.append(0)
    else:
        foo = test_runs_vs_year[i]
        test_runs.append(foo - test)
        test = foo
    years.append(i)


plt.figure(figsize=(10, 6))

ind = np.array(years)
dataset1 = np.array(odi_runs)
dataset2 = np.array(t20_runs)
dataset3 = np.array(test_runs)

p1 = plt.bar(ind, dataset1, label="ODIs", color='r')
p2 = plt.bar(ind, dataset2, label="T20Is", bottom=dataset1, color='b')
p3 = plt.bar(ind, dataset3, label="Tests", bottom=dataset1+dataset2, color='g')

plt.xticks(years)
plt.xlabel('Year')
plt.ylabel('Runs')
plt.legend()

plt.savefig('imgs/odi_t20_tests_runs.png')

plt.close()


odi_avg = []
test_avg = []
t20_avg = []
years = []
odi = 0
test = 0
t20 = 0
for i in range(2007, 2022):
    if i not in odi_avg_vs_years.keys():
        odi_avg.append(0)
    else:
        odi_avg.append(odi_avg_vs_years[i])
    if i not in t20_avg_vs_years.keys():
        t20_avg.append(0)
    else:
        t20_avg.append(t20_avg_vs_years[i])
    if i not in test_avg_vs_years.keys():
        test_avg.append(0)
    else:
        test_avg.append(test_avg_vs_years[i])
    years.append(i)


plt.figure(figsize=(10, 6))

ind = np.array(years)
dataset1 = np.array(odi_avg)
dataset2 = np.array(t20_avg)
dataset3 = np.array(test_avg)

p1 = plt.plot(list(odi_avg_vs_years.keys()), list(
    odi_avg_vs_years.values()), marker='o', label="ODIs", color='r')
p2 = plt.plot(list(t20_avg_vs_years.keys()), list(
    t20_avg_vs_years.values()), marker='o', label="T20Is", color='b')
p3 = plt.plot(list(test_avg_vs_years.keys()), list(
    test_avg_vs_years.values()), marker='o', label="Tests", color='g')

plt.xticks(years)
plt.xlabel('Year')
plt.ylabel('avg')
plt.legend()

plt.savefig('imgs/odi_t20_tests_avg.png')

plt.close()

plt.figure()
ind = ["ODIs", "T20Is", "Tests"]
dataset1 = np.array(
    [odi_4s_vs_year[2021], t20_4s_vs_year[2021], test_4s_vs_year[2021]])
dataset2 = np.array(
    [odi_6s_vs_year[2021], t20_6s_vs_year[2021], test_6s_vs_year[2021]])

p1 = plt.bar(ind, dataset1, label="Fours", color='r')
p2 = plt.bar(ind, dataset2, label="Sixes", bottom=dataset1, color='b')

plt.xticks(ind)
plt.xlabel('Format')
plt.ylabel('Count')
plt.title('Boundaries across all formats')
plt.legend()
plt.savefig('imgs/odi_t20_tests_boundaries.png')
plt.close()


plt.figure()
ind = ["ODIs", "T20Is", "Tests"]
dataset1 = np.array(
    [odi_50s_vs_year[2021], t20_50s_vs_year[2021], test_50s_vs_year[2021]])
dataset2 = np.array(
    [odi_100s_vs_year[2021], t20_100s_vs_year[2021], test_100s_vs_year[2021]])

p1 = plt.bar(ind, dataset1, label="Fifties", color='r')
p2 = plt.bar(ind, dataset2, label="Hundreds", bottom=dataset1, color='b')

plt.xticks(ind)
plt.xlabel('Format')
plt.ylabel('Count')
plt.legend()
plt.title('Fifties and Hundreds across all formats')

plt.savefig('imgs/odi_t20_tests_fifties_huns.png')
plt.close()
