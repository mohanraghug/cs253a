#!/bin/bash
rm processed_Covid_Data_170268.csv
touch processed_Covid_Data_170268.csv

states=($(jq -r 'keys | @sh' covid_Data.json | tr -d "'"))
j=0

for s in ${states[@]}; do
    
    # extracting the names of districts and parsing them
    districts=$(jq -r "[.[]] | .[$j].districts | keys" covid_Data.json)
    foo=", "
    bar=","
    districts=$(echo $districts | sed "s/$foo/$bar/g" | tr -d '"[]')
    districts="${districts:1:-1}"
    
    i=0
    fl=0
    OLD_IFS=$IFS
    IFS=","
    per=0

    for d in $districts; do
        
        confirmed=$(jq -r "[.[]] | .[$j].districts | [.[]] | .[$i].total.confirmed" covid_Data.json)
        recovered=$(jq -r "[.[]] | .[$j].districts | [.[]] | .[$i].total.recovered" covid_Data.json)
        
        if [[ $d != "Unknown" &&  $d != "Unknown " && $confirmed -ge 5000 ]]; then
            ((cur=recovered*10000/confirmed))
            if [[ $cur -ge $per ]]; then
                dis=$d
                per=$cur
                conf=$confirmed
                fl=1
            fi
        fi
        
        ((i=i+1))
    done
    
    IFS=$OLD_IFS


    if [[ $fl -eq 1 ]]; then
        ((int=per/100))
        ((dec=per%10))
        ((per=per/10))
        ((snd=per%10))
        echo $s,$dis,$conf,$int.$snd$dec >> processed_Covid_Data_170268.csv 
    fi

    ((j=j+1))
done
