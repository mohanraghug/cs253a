#include <bits/stdc++.h>

using namespace std;

mt19937 rng(chrono::steady_clock::now().time_since_epoch().count());

int getRand(int l, int r)
{
    uniform_int_distribution<int> uid(l, r);
    return uid(rng);
}

inline int64_t random_long(long long l, long long r)
{
    uniform_int_distribution<int64_t> generator(l, r);
    return generator(rng);
}

int main()
{
    for (int i = 0; i < 2; i++)
    {
        int x = getRand(INT_MIN, INT_MAX);
        cout << x << ' ';
    }
    cout << endl;

    return 0;
}