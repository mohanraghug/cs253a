#!/bin/bash
n=$(ls -l tests/ | wc -l)
((n=n-1))
k=$1
rm -rf reduced_tests
DIR=main
cd $DIR
mkdir bin
g++ analyzer.cpp -o bin/analyzer
g++ reducer.cpp -o bin/reducer
g++ comparer.cpp -o bin/comparer
./bin/analyzer $n
./bin/reducer $k
./bin/comparer $n
rm -rf *.gcov *.gcno *.gcda *.bb *.gcbb *.out
rm -rf P
rm -rf bin