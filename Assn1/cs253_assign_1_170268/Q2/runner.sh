#!/bin/bash
handler(){
    kill $(ps -o pid= --ppid $PID1)
    kill $(ps -o pid= --ppid $PID2)
    kill $(ps -o pid= --ppid $PID3) 
    exit 0
}

#mykill gets invoked when any of these signals are received.
trap handler SIGINT SIGTERM 

#storing pid for further use
echo $$ > pid

./10ZmoZg3pWmw9aA < $1 & 
PID1=($!) 
./DFwAU7iSoYorHy8 < $2 &
PID2=($!) 
./dUkeKOpRSOxQfx2 < $3 &
PID3=($!) 

wait


