#!/bin/bash

# remove directories from previous runs
for dir in */; do
   chmod 700 $dir
   rm -rf $dir
done

rm 170268_*


# run the preprocessing script to obtain the reqd directories and package name
output=$(./$1 | head -n 1)
outputarray=($output)
packagename=${outputarray[-1]} 
echo "package name is $packagename"

for dir in */; do
  chmod 700 $dir
  file_count=$(ls -1 $dir | wc -l)
  if [[ file_count -eq 1 ]]; then
    date_dir=$dir
  elif [[ file_count -eq 2 ]]; then
    id_dir=$dir
  fi
done

echo "date_directory is $date_dir"
echo "ids_direcory is $id_dir"

chmod 700 $2

./$2 $date_dir $packagename $id_dir 170268 > submission-q1.txt
cat submission-q1.txt
