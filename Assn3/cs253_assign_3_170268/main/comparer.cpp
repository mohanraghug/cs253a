#include <bits/stdc++.h>

using namespace std;

void clean_up()
{
    system("rm -rf *.gcov *.gcno *.gcda *.bb *.gcbb");
    system("rm -rf P");
    system("g++ -Wall -fprofile-arcs -ftest-coverage ../P.cpp -o P");
}

void run_test(int i)
{
    string s = "./P < ../tests/" + to_string(i) + ".in > /dev/null";
    system(s.c_str());
}

void run_reduced_tests(int i)
{
    string s = "./P < ../reduced_tests/" + to_string(i) + ".in > /dev/null";
    system(s.c_str());
}

int main(int argc,char* argv[])
{
    int n = stoi(argv[1]);

    clean_up();

    cout << "=================================" << endl;
    cout << "  GCOV output before reduction   " << endl;
    cout << "=================================" << endl;

    for (int i = 1; i <= n; i++)
    {
        run_test(i);
    }

    system("gcov -b -c P.cpp");

    clean_up();

    cout << "=================================" << endl;
    cout << "  GCOV output after reduction    " << endl;
    cout << "=================================" << endl;

    ifstream fin("reducer.out");

    int k;
    fin >> k;

    for (int i = 0; i < k; i++)
    {
        int x;
        fin >> x;
        run_reduced_tests(x);
    }

    system("gcov -b -c P.cpp");

    return 0;
}