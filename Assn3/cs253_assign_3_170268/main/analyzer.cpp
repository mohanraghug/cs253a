#include <bits/stdc++.h>

using namespace std;

ofstream fout("analysis.out");

void clean_up(){
    system("rm -rf *.gcov *.gcno *.gcda *.bb *.gcbb");
    system("rm -rf P");
}

void run_test(int i){
    system("g++ -Wall -fprofile-arcs -ftest-coverage ../P.cpp -o P");
    string s = "./P < ../tests/" + to_string(i) + ".in > /dev/null";
    system(s.c_str());
    system("gcov -b -c P.cpp > /dev/null");
}

int total_branches;

void count_branches(){
    clean_up();
    run_test(1);
    ifstream fin("P.cpp.gcov");
    total_branches = 0;
    string s;
    while (getline(fin, s))
    {
        istringstream ss(s);
        string t;
        ss >> t;

        if (t != "branch")
            continue;
        total_branches++;
    }
}

void analyze(){
    ifstream fin("P.cpp.gcov");
    string s;
    int cur = 0;
    vector<int> branches;
    while (getline(fin, s))
    {
        istringstream ss(s);
        string t;
        ss >> t;

        if(t != "branch")
            continue;
        cur++;
        ss >> t;
        ss >> t;
        if(t == "never")
            continue;
        ss >> t;
        int cnt = stoi(t);

        if(cnt > 0){
            branches.push_back(cur);
        }
    }
    fout << (int)branches.size() << endl;
    for(int x:branches)
        fout << x << ' ';
    fout << endl;
}

int main(int argc, char *argv[])
{
    int n = stoi(argv[1]);
    count_branches();
    fout << n << ' ' << total_branches << endl;

    for (int i = 1; i <= n; i++)
    {
        clean_up();
        run_test(i);
        analyze();
    }

    return 0;
}