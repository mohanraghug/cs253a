from flask import Flask, render_template, request, redirect, make_response
import string
import random

app = Flask(__name__)


users = {
    "Mohan": "foobar",
    "attacker": "qwerty",
    "Raghu": "password"
}

balances = {
    "Mohan": 100000,
    "attacker": 100,
    "Raghu": 10000
}

sessions = {
    "Mohan": set(),
    "attacker": set(),
    "Raghu": set()
}

status = {
    "Mohan": "",
    "attacker": "",
    "Raghu": "",
}
csrf_tokens = {}


@app.route("/")
def index():
    name = request.cookies.get('username')
    session_id = request.cookies.get('sid')
    if not name or not session_id in sessions[name]:
        return render_template("index.html")
    else:
        return render_template("hello.html", name=name, balance=balances[name], status=status[name], token=csrf_tokens[session_id])


@app.route("/login", methods=["POST"])
def login():
    username = request.form["username"]
    password = request.form["password"]
    if username in users and users[username] == password:
        response = make_response(redirect("/"))
        session_id = ''.join(random.choices(string.ascii_letters, k=100))
        csrf_token = ''.join(random.choices(string.ascii_letters, k=100))
        csrf_tokens[session_id] = csrf_token
        response.set_cookie('username', username, samesite="Lax")
        response.set_cookie('sid', session_id, samesite="Lax")
        sessions[username].add(session_id)
        return response
    else:
        return "Login Failed"


@app.route("/logout")
def logout():
    name = request.cookies.get('username')
    session_id = request.cookies.get('sid')
    csrf_tokens.pop(session_id)
    response = make_response(redirect("/"))
    if name in users and session_id in sessions[name]:
        sessions[name].remove(session_id)
    response.set_cookie('username', "", expires=0)
    response.set_cookie('sid', "", expires=0)
    return response


@app.route("/send", methods=["POST"])
def send():
    name = request.cookies.get('username')
    sid = request.cookies.get('sid')
    response = make_response(redirect("/"))
    to = request.form["to"]
    amount = int(request.form["amount"])
    
    if not name or not sid or "csrf_token" not in request.form.keys():
        status[name] = f"csrf detected, be sure to close all malicious websites"
        return "forbidden", 403
    csrf_token = request.form["csrf_token"]

    if name in users and sid in sessions[name] and balances[name] >= amount and to in users and csrf_token == csrf_tokens[sid]:
        balances[name] -= amount
        balances[to] += amount
        status[name] = f"Successfully transferred Rs.{amount} to {to}"
    elif not csrf_token or csrf_token != csrf_tokens[sid]:
        status[name] = f"csrf detected, be sure to close all malicious websites"
    elif not to in users:
        status[name] = f"failed due to invalid beneficiary username {to}"
    elif amount > balances[name]:
        status[name] = f"failed due to insufficient funds."
    return response
