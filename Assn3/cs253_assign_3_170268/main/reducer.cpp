#include <bits/stdc++.h>

using namespace std;

int main(int argc, char *argv[])
{

    ifstream fin("analysis.out");
    ofstream cout("reducer.out");

    int T, B;
    fin >> T >> B;

    vector<int> tests[B], branches[T], cnt(T);

    for (int t = 0; t < T; t++)
    {
        int k;
        fin >> k;
        cnt[t] = k;

        for (int j = 0; j < k; j++)
        {
            int x;
            fin >> x;
            tests[x].push_back(t);
            branches[t].push_back(x);
        }
    }

    int K = stoi(argv[1]);

    set<pair<int, int>, greater<pair<int, int>>> s;

    for (int t = 0; t < T; t++)
    {
        s.insert({cnt[t], t});
    }

    system("rm -rf ../reduced_tests");
    system("mkdir ../reduced_tests");

    vector<int> final;

    while (K > 0 and !s.empty())
    {
        auto pp = *s.begin();
        s.erase(s.begin());
        K--;
        int t = pp.second, _ = pp.first;
        final.push_back(t+1);
        string cmd = "cp ../tests/" + to_string(t + 1) + ".in ../reduced_tests/" + to_string(t + 1) + ".in";
        system(cmd.c_str());

        cnt[t] = 0;
        for (int b : branches[t])
        {
            for (int tt : tests[b])
            {
                if (s.count(make_pair(cnt[tt], tt)))
                {
                    s.erase(make_pair(cnt[tt], tt));
                    cnt[tt]--;
                    if (cnt[tt] > 0)
                    {
                        s.insert(make_pair(cnt[tt], tt));
                    }
                }
            }
            tests[b].clear();
        }
    }

    cout << int(final.size()) << endl;
    for (int x : final)
    {
        cout << x << endl;
    }

    return 0;
}